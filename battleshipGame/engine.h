#ifndef ENGINE
#define ENGINE

#include<SFML/Graphics.hpp>
#include<vector>

#include"multiplayer.h"
#include"singleplayer.h"
#include"variables.h"
//#include"ship.h"

class Engine {
	gameType typeOfGame = (gameType)typeNone;

public:
	Engine(){}

	void run()
	{
		backgroundTexture.loadFromFile("backgroundTexture.png");
		background.setTexture(&backgroundTexture);

		while (window.isOpen())
		{

			while (window.pollEvent(evnt))
			{
				window.clear();
				window.draw(background);
				beginning();
				changeTypeOfGameWhenClicked();

				if (changeTypeOfGameWhenClicked() == 1)
				{
					while (window.isOpen())
					{
						while (window.pollEvent(evnt))
						{
							window.clear();
							window.draw(background);
							chooseTypeOfGame();
							window.display();

							switch (evnt.type)
							case sf::Event::Closed:
									window.close();
						}
					}
				}
				window.display();

				switch (evnt.type)
				case sf::Event::Closed:
					window.close();
			}
		}
	}
private:
	void changeColorOfAButton();
	bool changeTypeOfGameWhenClicked();
	void beginning();
	void chooseTypeOfGame();
};

void Engine::changeColorOfAButton()
{
	sf::Vector2f mouse = (sf::Vector2f)(sf::Mouse::getPosition(window));

	if (singleplayerButton.getGlobalBounds().contains(mouse))
		singleplayerButton.setFillColor(sf::Color::Cyan);
	else
		singleplayerButton.setFillColor(sf::Color::White);

	if (multiplayerButton.getGlobalBounds().contains(mouse))
		multiplayerButton.setFillColor(sf::Color::Cyan);
	else
		multiplayerButton.setFillColor(sf::Color::White);

}

bool Engine::changeTypeOfGameWhenClicked()
{
	bool answer = 0;
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		if ((singleplayerButton.getGlobalBounds().contains(evnt.mouseButton.x, evnt.mouseButton.y)))
		{
			typeOfGame = typeSingleplayer;
			answer = 1;
		}
		if (multiplayerButton.getGlobalBounds().contains(evnt.mouseButton.x, evnt.mouseButton.y))
		{
			typeOfGame = typeMultiplayer;
			answer = 1;
		}
	}
	return answer;
}

void Engine::beginning()
{
	singleplayerButton.setPosition(windowSize / 3, windowSize / 2);
	singleTexture.loadFromFile("singleplayerTexture.png");
	singleplayerButton.setTexture(&singleTexture);

	multiplayerButton.setPosition(2 * windowSize / 3, windowSize / 2);
	multiTexture.loadFromFile("multiplayerTexture.png");
	multiplayerButton.setTexture(&multiTexture);

	if (typeOfGame == typeNone)
	{
		window.draw(singleplayerButton);
		window.draw(multiplayerButton);
	}
}

void Engine::chooseTypeOfGame()
{
	if (typeOfGame == typeSingleplayer)
	{
		SingleplayerScreen scr;
		if (newGame == 0)
		{
			newSingleplayerScreen(scr);
//			scr.createEnemyBoard2();
			newGame = 1;
		}


//		scr.createEnemyBoard2();
		scr.printBoard2();
	}

	if (typeOfGame == typeMultiplayer)
	{
		MultiplayerScreen scr;
		scr.printBoard2();
	}

}
#endif