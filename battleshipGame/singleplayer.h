#ifndef SINGLEPLAYER
#define SINGLEPLAYER

#include<SFML/Graphics.hpp>

#include"screen.h"

enum boardType { player, enemy };

class SingleplayerScreen : public Screen {
public:
	SingleplayerScreen(boardType type) 
	{
		if (type == enemy)
			createEnemyBoard2();
		// else create player, random or chosen
	}
	SingleplayerScreen() {}

	void printBoard1(void);
	void printBoard2(void);
	void createEnemyBoard2();

};

void SingleplayerScreen::printBoard1()
{
	drawMatrixOfShips(Board1);
	printHeadline();
}
void SingleplayerScreen::printBoard2()
{
	drawMatrixOfShips(Board2);
	printHeadline();
}
void SingleplayerScreen::createEnemyBoard2()
{
	Board2 = makeMatrixOfShips(sizeOfMap);
	setAllTheShips(Board2);
}

void newSingleplayerScreen(SingleplayerScreen &scr)
{
//	SingleplayerScreen scr(enemy);
	if (newGame == 0)
	{
		scr.createEnemyBoard2();
		newGame = 1;
	}
//	return scr;
}

#endif