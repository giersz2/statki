#ifndef VARIABLES
#define VARIABLES

#include<SFML/Graphics.hpp>
//#include"singleplayer.h"

extern int windowSize;

extern sf::RenderWindow window;
extern sf::Event evnt;

enum gameType { typeNone, typeSingleplayer, typeMultiplayer };

extern sf::RectangleShape singleplayerButton;
extern sf::Texture singleTexture;

extern sf::RectangleShape multiplayerButton;
extern sf::Texture multiTexture;

extern sf::RectangleShape background;
extern sf::Texture backgroundTexture;

extern int sizeOfMap;
extern int sizeOfMapInPixels;

extern bool newGame;

//extern SingleplayerScreen scr;


#endif