#include"variables.h"

#include<SFML/Graphics.hpp>

int windowSize = 1024;



sf::RenderWindow window(sf::VideoMode(windowSize, windowSize), "battleship game beta", sf::Style::Default);
sf::Event evnt;

sf::RectangleShape singleplayerButton(sf::Vector2f(100.0f, 100.f));
sf::Texture singleTexture;

sf::RectangleShape multiplayerButton(sf::Vector2f(100.0f, 100.f));
sf::Texture multiTexture;

sf::RectangleShape background(sf::Vector2f(windowSize, windowSize));
sf::Texture backgroundTexture;

int sizeOfMap = 10;
int sizeOfMapInPixels = 510;

//SingleplayerScreen scr;


bool newGame = 0;