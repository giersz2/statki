#ifndef SHIP
#define SHIP

#include<SFML/Graphics.hpp>
#include<vector>

#include"engine.h"
#include"variables.h"

float shipWidth = 50.0f;
float shipHeight = 50.0f;
float outlineThickness = 5.0f;

sf::RectangleShape ship(sf::Vector2f(shipWidth, shipHeight));

// potrzebne???
float pos1 = outlineThickness;
float pos2 = outlineThickness;

enum eType { nothing, occupied, shot, missed, notpossible };

class Ship {
	int x;
	int y;
	eType type;

public:
	Ship(int x, int y)
	{
		this->x = x;
		this->y = y;
		setType((eType)nothing);
	}
	int getX() { return x; }
	int getY() { return y; }
	eType getType() { return type; }
	void setType(eType t) { type = t; }
	void attack();
	void drawShip();
	void putShip();
	void drawShips(int n);
//	std::vector<std::vector<Ship>> makeMatrixOfShips(int n);
//	void drawMatrixOfShips(std::vector<std::vector<Ship>>v);

};

// naglowek funkcji poza klasa Ship
std::vector<std::vector<Ship>> makeMatrixOfShips(int n);
void drawMatrixOfShips(std::vector<std::vector<Ship>>v);

bool isThereAnyShip(std::vector<std::vector<Ship>> &v, int a, int b, int dir, int sizeOfShip);
void setBiggerRandomShips(std::vector<std::vector<Ship>> &v, int sizeOfShip);
void setAllTheShips(std::vector<std::vector<Ship>> &v);

void Ship::attack()
{
	setType((eType)shot);
}
void Ship::drawShip()
{
	sf::RectangleShape shipTemp(sf::Vector2f(shipWidth, shipHeight));
	shipTemp.setPosition(sf::Vector2f(outlineThickness + shipWidth*x, outlineThickness + shipHeight*y));
	shipTemp.setOutlineThickness(outlineThickness);
	shipTemp.setOutlineColor(sf::Color(165, 210, 219));
	if (type == nothing)
		shipTemp.setFillColor(sf::Color::Transparent);
	if (type == occupied)
		shipTemp.setFillColor(sf::Color(137, 113, 185));
	if (type == shot)
		shipTemp.setFillColor(sf::Color::Red);
	if (type == notpossible)
		shipTemp.setFillColor(sf::Color::Magenta);
	window.draw(shipTemp);
}
void Ship::putShip()
{
	setType((eType)occupied);
}
void Ship::drawShips(int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			ship.setPosition(sf::Vector2f(outlineThickness + shipWidth*i, outlineThickness + shipHeight*j));
			window.draw(ship);
		}
	}
}

// definicje funkcji poza klas� Ship:
std::vector<std::vector<Ship>> makeMatrixOfShips(int n)
{
	std::vector<std::vector<Ship>> v;
	for (int i = 0; i < n; i++)
	{
		std::vector<Ship> row;
		for (int j = 0; j < n; j++)
		{
			row.push_back(Ship(j, i));
		}
		v.push_back(row);
	}
	return v;
}
void drawMatrixOfShips(std::vector<std::vector<Ship>>v)
{
	for (std::vector<std::vector<Ship>>::iterator it = v.begin(); it != v.end(); it++)
	{
		for (std::vector<Ship>::iterator it2 = (*it).begin(); it2 != (*it).end(); it2++)
		{
			(*it2).drawShip();
		}
	}
}





bool isThereAnyShip(std::vector<std::vector<Ship>> &v, int a, int b, int dir, int sizeOfShip)
{
	bool answer = 0;
	if (dir == 1)
	{
		for (int i = a; i < (a + sizeOfShip); i++)
			if (v[i][b].getType() == occupied)
				answer = 1;
	}
	else
	{
		for (int i = b; i < (b + sizeOfShip); i++)
			if (v[a][i].getType() == occupied)
				answer = 1;
	}
	return answer;
}
void setBiggerRandomShips(std::vector<std::vector<Ship>> &v, int sizeOfShip)
{
	int a = rand() % sizeOfMap;
	int b = rand() % sizeOfMap;

	int dir = rand() % 2;
	if (dir == 1)
		if ((a + sizeOfShip) <= sizeOfMap && isThereAnyShip(v, a, b, dir, sizeOfShip) == 0)
			//&& isAnyShipAround(v, a, b, dir, sizeOfShip) == 0)
			for (int i = 0; i < sizeOfShip; i++)
			{
				if ((v[a][b]).getType() == nothing)
				{
					v[a][b].putShip();
					a++;
				}
				else
				{
					a = rand() % sizeOfMap;
					b = rand() % sizeOfMap;
					i = 0;
				}
			}
		else
			setBiggerRandomShips(v, sizeOfShip);
	else
		if ((b + sizeOfShip) <= sizeOfMap && isThereAnyShip(v, a, b, dir, sizeOfShip) == 0)
			//&& isAnyShipAround(v, a, b, dir, sizeOfShip) == 0)
			for (int i = 0; i < sizeOfShip; i++)
			{
				if ((v[a][b]).getType() == nothing)
				{
					v[a][b].putShip();
					b++;
				}
				else
				{
					a = rand() % sizeOfMap;
					b = rand() % sizeOfMap;
					i = 0;
				}
			}
		else
			setBiggerRandomShips(v, sizeOfShip);
}
void setAllTheShips(std::vector<std::vector<Ship>> &v)
{
	int numberOfShips = 1;
	int size = 5;
	while (size > 0)
	{
		for (int i = 0; i < numberOfShips; i++)
		{
			setBiggerRandomShips(v, size);
		}
		size--;
		numberOfShips++;
	}
}

#endif