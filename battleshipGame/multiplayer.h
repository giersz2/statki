#ifndef	MULTIPLAYER
#define MULTIPLAYER

#include"screen.h"

class MultiplayerScreen : public Screen {
public:
	void printBoard1(void);
	void printBoard2(void);
};

void MultiplayerScreen::printBoard1()
{
	drawMatrixOfShips(Board1);
	printHeadline();
}
void MultiplayerScreen::printBoard2()
{
	drawMatrixOfShips(Board2);
	printHeadline();
}

#endif