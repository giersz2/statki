#ifndef SCREEN
#define SCREEN

#include<SFML/Graphics.hpp>
#include"ship.h"

class Screen {
protected:
	std::vector<std::vector<Ship>> Board1; 
	std::vector<std::vector<Ship>> Board2;
	void printHeadline();

public:
	virtual void printBoard1() = 0;
	virtual void printBoard2() = 0;


};

// nie dziala
void Screen::printHeadline()
{
	sf::RectangleShape headline(sf::Vector2f((float)sizeOfMapInPixels, 50.0f));
	headline.setPosition(0.0f, sizeOfMapInPixels);

	sf::Text text;
	text.setFillColor(sf::Color::Red);
	text.setString("Player");
//	text.setPosition(headline.getPosition());
	text.setCharacterSize(50);
	text.setStyle(sf::Text::Bold);

	window.draw(headline);
	window.draw(text);
}

#endif